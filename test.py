# class Product:

#     def __init__(self, price, price_id, quantity):
#         self.price = price
#         self.price_id = price_id
#         self.quantity = quantity
    
#     def __repr__(self):
#         return f'product price-{self.price}, id-{self.price_id}, quantity-{self.quantity}'

#     def buy(self, quan):
#         self.quan = quan
#         if quan >= self.quantity:
#             raise Exception('you can not buy more product')
#         elif quan <= self.quantity:
#             self.quantity = self.quantity - quan

#     def __eq__(self, second) -> bool:
#         if self.price == second.price and self.price_id == second.price_id and self.quantity == second.quantity:
#             return True
#         return False
    
# product1 = Product(1000, 56, 5)
# product2 = Product(1000, 57, 5)
# print(product1 == product2)
# product1.buy(3)
# print(product1.quantity)

from datetime import datetime


class Patient:
    def __init__(self, name, surname, age, gender) -> None:
        self.name = name
        self.surname = surname
        self.age = age
        self.gender = gender
    
    def __repr__(self) -> str:
        return f'{self.name} {self.surname} - {self.gender}, {self.age} years old'

class Doctor:
    
    def __init__(self, name, surname, schedul) -> None:
        self.name = name
        self.surname = surname
        self.schedul = schedul
    
    def register(self, datetime, patient):
        self.schedul[datetime] = patient

    def __repr__(self) -> str:
        return f'{self.name} {self.surname} schedule - {self.schedul}'
        
        
Patient2 = Patient('Alex', 'Jane', 25, 'M')
Patient1 = Patient('Artur','Jhon', 25, 'M')
Doctor1 = Doctor('Robert', 'Jhonshon', {datetime(2022, 5, 18, 15, 30).isoformat(): Patient1.name}) 

Doctor1.register(datetime(2022, 6, 18, 15, 30).isoformat(), Patient2.name)
print(Doctor1)